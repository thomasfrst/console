﻿using System;
using System.Collections.Generic;

namespace Console
{
	public abstract class Console
	{
		#region Variables

		protected ConsoleDatabase database;

		// Commands we call in the past.
		// Need to be updated every command call in the cmd line!
		protected List<ConsoleCommand> history;

		#endregion

		public Console ()
		{
			history = new List<ConsoleCommand> ();
		}

		#region Methods

		// Processes data from input
		public abstract void Read (string data);

		public abstract void Write (string data);

		public bool RegisterCommand (ConsoleCommand cmd)
		{
			return database.AddCommand (cmd);
		}
			
		public bool UnregisterCommand (ConsoleCommand cmd)
		{
			return database.RemoveCommand (cmd);
		}
			
		public void RunCommand (string command, bool writeToConsole = false)
		{
			if (writeToConsole) 
			{
				// Write commmand to the console
				Write (command);
			}

			// Perform the command
			Read (command);
		}
			
		public List<string> GetHint (string word)
		{
			// Return hint from the database
			return database.GetHint (word);
		}

		public List<ConsoleCommand> GetCommand(string cmd)
		{
			return database.FindCommand (cmd);
		}

		public List<ConsoleCommand> GetAllCommands ()
		{
			return database.GetAllCommands ();
		}


		#endregion
	}
}

