﻿using System;
using System.Collections.Generic;

namespace Console
{
	public abstract class ConsoleDatabase
	{
		public abstract bool AddCommand(ConsoleCommand cmd);

		public abstract bool RemoveCommand(ConsoleCommand cmd);

		public abstract List<ConsoleCommand> FindCommand(string cmd);

		public abstract List<ConsoleCommand> GetAllCommands ();

		public abstract List<string> GetHint (string word);
	}
}

