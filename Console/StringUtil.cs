﻿using System;
using System.Collections.Generic;

namespace Console
{
	public static class StringUtil
	{
		public static List<string> GetArgs(string input)
		{
			List<string> res = new List<string> ();

			if (!input.Contains (" ")) 
			{
				return res;
			}

			string[] split = input.Split (' ').SubArray (1);
			res = new List<string> (split);
			return res;
		}

		public static string GetCmd(string input)
		{
			if (!input.Contains (" ")) 
			{
				return input;
			}

			string res = input.Substring (0, input.IndexOf (' '));
			return res;
		}

		public static T[] SubArray<T>(this T[] data, int index, int length = -1)
		{
			if (length == -1) 
			{
				length = data.Length - index;
			}

			T[] result = new T[length];
			Array.Copy(data, index, result, 0, length);
			return result;
		}
	}
}

