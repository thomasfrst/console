﻿using System;
using System.Collections.Generic;

namespace Console
{
	public abstract class ConsoleCommand
	{
		// Name of the command
		protected string command;

		// Arguments for the command
		protected List<string> arguments;

		public string Command 
		{
			get { return command; }
		}

		public List<string> Args 
		{
			get { return arguments; }
		}

		public abstract void Perform(List<string> args);

		public bool Perform()
		{
			if (arguments != null) 
			{
				Perform (arguments);
				return true;
			}

			return false;
		}
	}
}

