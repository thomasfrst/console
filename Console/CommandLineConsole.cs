﻿using System;
using System.Collections.Generic;

namespace Console
{

	public class CommandLineConsole : Console
	{
		
		#region Constructors

		public CommandLineConsole()
		{
			database = new TrieDB ();
			ConsoleInternalCommands.RegisterCommands (this);
		}

		#endregion

		#region implemented abstract members of Console

		// Processes input.
		public override void Read (string data)
		{
			if (data.Length == 0)
				return;

			List<string> args = StringUtil.GetArgs (data);
			string cmd = StringUtil.GetCmd (data);

			List<ConsoleCommand> commands = database.FindCommand (cmd);
			if (commands != null && commands.Count > 0)
			{
				foreach (var command in commands) 
				{
					command.Perform (args);
					history.Add (command);
				}
			} 
			else 
			{
				Write ("Error: Unrecognized command.");
			}
		}
			
		public override void Write (string data)
		{
			System.Console.WriteLine (data);
		}

		#endregion
		
	}
}

