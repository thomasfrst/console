﻿using System;
using System.Collections.Generic;

namespace Console
{
	public class TrieDBNode
	{
		private char c;

		private List<ConsoleCommand> commands = new List<ConsoleCommand> ();

		private TrieDBNode parent = null;

		private List<TrieDBNode> children = new List<TrieDBNode> ();

		public TrieDBNode Parent
		{
			get { return parent; }
			set { parent = value; }
		}

		public char C 
		{
			get { return c; }
		}

		public List<ConsoleCommand> Commands 
		{
			get { return commands; }
		}

		public List<TrieDBNode> Childs
		{
			get { return children; }
		}

		public TrieDBNode (char c)
		{
			this.c = c;
		}

		public bool AddChild (char c)
		{
			if (GetChild (c) != null)
				return false;

			children.Add (new TrieDBNode (c));
			return true;
		}

		public bool AddChild (TrieDBNode node)
		{
			foreach (var child in children) 
			{
				if (child.c == node.c) 
				{
					foreach (var cmd in node.commands) 
					{
						child.AssignCommand (cmd);
					}
					return false;
				}
					
			}

			children.Add (node);

			return true;
		}

		public bool RemoveChild(char c)
		{
			for (int i = 0; i < children.Count; i++) 
			{
				if (children [i].c == c) 
				{
					children.RemoveAt (i);
					return true;
				}
			}

			return false;
		}

		public TrieDBNode GetChild(char c) 
		{
			foreach (var child in children) 
			{
				if (child.c == c)
					return child;
			}

			return null;
		}

		public bool AssignCommand(ConsoleCommand cmd)
		{
			if (commands.Contains (cmd))
				return false;

			commands.Add (cmd);
			return true;
		}

		public bool UnassignCommand(ConsoleCommand cmd)
		{
			if (!commands.Contains (cmd))
				return false;

			commands.Remove (cmd);
			return true;
		}
	}
}

