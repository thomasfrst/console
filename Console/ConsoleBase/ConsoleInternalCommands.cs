﻿using System;
using System.Collections.Generic;

namespace Console
{
	public class ConsoleInternalCommands
	{
		public static void RegisterCommands(Console console)
		{
			console.RegisterCommand (new ConsoleActionCommand(delegate (List<string> args)
			{
				if (args.Count > 0)
				{
					console.Write ("Use help withou arguments.");
					return;
				}

				List<ConsoleCommand> cmds = console.GetAllCommands ();
				console.Write ("Registered commands: " + cmds.Count);

				foreach (var cmd in cmds) {
					console.Write ("\t" + cmd.Command);
				}
			}, "help"));

			console.RegisterCommand (new ConsoleActionCommand(delegate (List<string> args)
			{
				if (args.Count > 0)
				{
					console.Write ("Error: Command require no argument.");
					return;
				}

				System.Console.Clear ();
			}, "clear"));

			console.RegisterCommand (new ConsoleActionCommand(delegate (List<string> args)
			{
				Environment.Exit (0);
			}, "exit"));
		}
	}
}

