﻿using System;
using System.Threading;
using System.Collections.Generic;

namespace Console
{
	class MainClass
	{
		static int x = 0;
		private static CommandLineConsole console;

		public static void Main (string[] args)
		{
			console = new CommandLineConsole ();

//			console.RegisterCommand (new TestCommand ("Command1"));
//			console.RegisterCommand (new TestCommand ("Comd1"));
//			console.RegisterCommand (new TestCommand ("TestWord"));
//			console.RegisterCommand (new TestCommand ("SpecialCmd"));

			console.RegisterCommand (new ConsoleActionCommand(SetVar, "testVar"));
			console.RegisterCommand (new ConsoleActionCommand(SetX, "x"));

			console.Write ("Welcome to Command Line Console! Type 'help' to see commands.");

			while (true)
			{
				string line = System.Console.ReadLine ();
				console.Read (line);
			}
		}

		static void SetVar(List<string> args)
		{
			if (args.Count == 0)
			{
				console.Write ("No arguments for this command");
			}
			else 
			{
				console.Write ("Arguments are: ");
				foreach (string arg in args) 
				{
					console.Write (arg);
				}
			}
		}

		static void SetX(List<string> args)
		{
			if (args.Count == 0)
			{
				console.Write (x.ToString ());
				return;
			}

			if (args.Count == 2)
			{
				if (args[0] == "=")
				{
					int tmp;

					if (int.TryParse (args[1], out tmp))
					{
						x = tmp;
						return;
					}

					console.Write ("Error: Cannot convert argument into number.");
					return;
				}
			} 
				
			console.Write ("Error: You need two arguments to call this command.");
		}
	}
}
