﻿using System;
using System.Collections.Generic;

namespace Console
{
	public class ConsoleActionCommand : ConsoleCommand
	{
		#region Members

		public delegate void ConsoleAction(List<string> args);

		private ConsoleAction action;

		#endregion

		#region Constrcutors

		public ConsoleActionCommand (ConsoleAction action, string name)
		{
			this.action = action;
			this.command = name;
		}

		#endregion
			
		#region implemented abstract members of ConsoleCommand

		public override void Perform (List<string> args)
		{
			action (args);
		}

		#endregion
	}
}

