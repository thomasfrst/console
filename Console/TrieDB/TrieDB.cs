﻿using System;
using System.Collections.Generic;

namespace Console
{
	public class TrieDB : ConsoleDatabase
	{
		#region Variables

		protected TrieDBNode root;

		protected const char separator = '.';

		#endregion

		#region Constructors

		public TrieDB ()
		{
			root = new TrieDBNode (separator);
		}

		#endregion

		#region Methods


		// Returns all commands starting with specified word.
		public List<ConsoleCommand> GetCommandsFrom(string word)
		{
			TrieDBNode fresh = root;

			for (int i = 0; i < word.Length; i++) 
			{
				fresh = fresh.GetChild (word [i]);
				if (fresh == null)
					return null;
			}

			return GetCommandsFrom (fresh);
		}

		// Returns all commands find from specified starting point.
		public List<ConsoleCommand> GetCommandsFrom(TrieDBNode node)
		{
			List<ConsoleCommand> commands = new List<ConsoleCommand> ();
			DFS (node, ref commands);
			return commands;
		}

		// Depth first search
		protected void DFS(TrieDBNode node, ref List<ConsoleCommand> commands)
		{
			if (node.Commands.Count != 0)
				foreach (var cmd in node.Commands)
					commands.Add (cmd);

			foreach (var child in node.Childs)
			{
				DFS (child, ref commands);
			}
		}
			
		public override string ToString ()
		{
			string res = "{ ";
			List<ConsoleCommand> cmds = GetAllCommands ();
			foreach (var cmd in cmds) 
			{
				res += cmd.Command + ", ";
			}

			res += "}";

			return res;
		}

		#endregion

		#region implemented abstract members of ConsoleDatabase

		public override bool AddCommand (ConsoleCommand cmd)
		{
			TrieDBNode fresh = root;
			TrieDBNode tmp = null;

			for (int i = 0; i < cmd.Command.Length; i++) 
			{
				tmp = fresh.GetChild (cmd.Command [i]);
				if (tmp == null) 
				{
					tmp = new TrieDBNode (cmd.Command [i]);
					fresh.AddChild (tmp);
					fresh = tmp;
				}

				fresh = tmp;
			}

			fresh.AssignCommand (cmd);
			return true;
		}
			
		public override bool RemoveCommand (ConsoleCommand cmd)
		{
			throw new NotImplementedException ();
		}
			
		public override List<ConsoleCommand> FindCommand (string cmd)
		{
			TrieDBNode fresh = root;

			for (int i = 0; i < cmd.Length; i++) 
			{
				fresh = fresh.GetChild (cmd [i]);
				if (fresh == null)
					return null;
			}

			return fresh.Commands;
		}
			
		// Returns hint from specific starting point of the command.
		public override List<string> GetHint(string start)
		{
			List<string> res = new List<string> ();

			List<ConsoleCommand> cmds = GetCommandsFrom (start);

			foreach (var cmd in cmds) 
			{
				res.Add (cmd.Command);
			}

			return res;
		}

		public override List<ConsoleCommand> GetAllCommands()
		{
			return GetCommandsFrom (root);
		}

		#endregion
	}
}

